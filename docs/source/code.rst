CanESM source code 
==================

.. note::
    This content is convered in `Screencast 1 <https://www.youtube.com/watch?v=h6cEF-Q7-Qw>`_


Availablity and licensing
+++++++++++++++++++++++++

The full CanESM source code, including all utilities is published as open source,
under the
`Open Government License Canada <https://open.canada.ca/en/open-government-licence-canada>`_,
and is available at https://gitlab.com/cccma/canesm.

Download the source code
++++++++++++++++++++++++

The CanESM source code is version controlled using `git <https://git-scm.com/>`_, where it is 
structured as a "super-repo" containing multiple sub-repositories, as described :ref:`below<Code Structure>`
Therefore, the most effective way to get the CanESM source code is to recursively clone the super repo:

.. code-block:: shell

    git clone --recursive git@gitlab.com:cccma/canesm.git               # to clone from the public facing repository
    git clone --recursive git@gitlab.science.gc.ca:CanESM/CanESM5.git   # to clone from the ECCC gitlab server

For users who wish to contribute to the development of CanESM are directed to the 
:ref:`contributing guide<Contributing to CanESM (Developers guide)>`.

Languages and dependencies
++++++++++++++++++++++++++

The model source code is almost exlusively Fortran. There is a mix of free and fixed form Fortran in CanAM,
while the other components are freeform. Other utilities and scripts are written in shell (bash), Python and
Perl.

* All component require a fortran compiler (GNU & Intel are tested).
* CanAM depends on MPI, openmp, and BLAS / LAPACK math libraries
* NEMO depends on Perl, NetCDF and MPI
* CanCPL depends on NetCDF, MPI and `ESMF <https://www.earthsystemcog.org/projects/esmf/>`_.  
    

Code structure
++++++++++++++

To maintain modularity, the code is organized such that each model component has a
dedicated git repository for the version control of its source code. A dedicated
super-repository tracks each of the components as git submodules. In this way,
the super-repo keeps track of which specific versions of each component combine
together to form a functional version of CanESM. A commit of the CanESM
super-repo., which is representable by an eight-character truncated SHA1 checksum,
hence uniquely defines a version of the full CanESM source code.

This CanESM5 repository is referred to as the super-repo. There are six directories
at the top level of the super-repo:

============= ============================================================================================================
Component     Description
============= ============================================================================================================
 CanAM        The Canadian Atmosphere Model source code (also see CanDIAG). This is code strictly used by the model alone.

 CanDIAG      The AGCM diagnostics code, also including all code which overlaps between diagnostics and CanAM.

 CanCPL       The "new" coupler, built for CanAM-CanNEMO.

 CanNEMO      The CCCma configuration of NEMO.

 CCCma_tools  All scripts and utilities, which effectively make up the "CCCma environment".

 CONFIG       Example make_jobs and basefiles (this is not a submodule).
============= ============================================================================================================

The first five directories (CanAM, CanDIAG, CanCPL, CanNEMO and CCCma_tools) are git submodules.
That means each of these directories is an independent git repository, and is tracked by
git in the super-repo. The code in each submodule and the super-repo must be treated as separate
repositories, as described in the :ref:`contributing guide<Contributing to CanESM (Developers guide)>`. 

