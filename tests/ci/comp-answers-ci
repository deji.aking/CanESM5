#!/bin/bash
#
#   COMP ANSWERS CI
#
#   This script compares the answers produced in the run stage
#   to the control answers and fails if the answers are different
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set -ex

# source global ci config vars
source ./tests/ci/ci.conf

# check for necessary config vars and set downstream ones
[[ -z $IDENTIFIER ]]        && { echo "IDENTIFIER must be defined"; exit 1; }
[[ -z $RUN_CONFIG ]]        && { echo "RUN_CONFIG must be defined"; exit 1; }
[[ -z $TEST_RUN_DIR ]]      && { echo "TEST_RUN_DIR must be defined"; exit 1; }
[[ -z $DAYS_TO_RUN ]]       && { echo "DAYS_TO_RUN must be defined"; exit 1; }
RUNID=ci-${IDENTIFIER}-${CI_COMMIT_SHA:0:7}
RUN_DIRECTORY=${TEST_RUN_DIR}/${RUNID}

# set ids to easily differentiate between development and control files
DEV_ID=${RUNID}.${DAYS_TO_RUN}days
CONTROL_ID=${IDENTIFIER}.${DAYS_TO_RUN}days
DEV_ANSWER_LOCATION=${RUN_DIRECTORY}/answers
CONTROL_ANSWER_LOCATION=${RUN_DIRECTORY}/CanESM_source_link/tests/ci/answers

# Depending on the config, we only compare certain files - set switches
case $RUN_CONFIG in
     ESM) comp_agcm=1; comp_ocean=1; ;;
    AMIP) comp_agcm=1; comp_ocean=0; ;;
    OMIP) comp_agcm=0; comp_ocean=1; ;;
esac

#~~~~~~~~~~~~
# Compare!
#~~~~~~~~~~~~
function are_identical(){
    local _file_1=$1
    local _file_2=$2
    local _return_status
    if cmp -s ${_file_1} ${_file_2}; then
        _return_status=0
    else
        _return_status=1
    fi
    return ${_return_status}
}
echo ""
echo "Comparing answers..."
echo ""
answer_diffs=0
if (( comp_ocean == 1 )); then
    ocean_files_missing=0
    file_prefix="final.state"
    dev_file=${DEV_ANSWER_LOCATION}/${file_prefix}.${DEV_ID}
    control_file=${CONTROL_ANSWER_LOCATION}/${file_prefix}.${CONTROL_ID}
    if ! [[ -e $dev_file ]]; then
        echo ""
        echo "-----------------------------------"
        echo "$dev_file doesn't exist!"
        echo "-----------------------------------"
        echo ""
        ocean_files_missing=1
    fi
    if ! [[ -e $control_file ]]; then
        echo ""
        echo "-----------------------------------"
        echo "$control_file doesn't exist!"
        echo "-----------------------------------"
        echo ""
        ocean_files_missing=1
    fi
    if (( ocean_files_missing == 0 )); then
        if are_identical $dev_file $control_file; then
            echo ""
            echo "OCEAN STATE IDENTICAL"
            echo ""
        else
            echo ""
            echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            echo "            DIFFERENCES IN THE OCEAN STATE DETECTED"
            echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            echo ""
            answer_diffs=1
        fi
    fi
fi
if (( comp_agcm == 1 )); then
    agcm_files_missing=0
    file_prefix="agcm.final.state"
    dev_file=${DEV_ANSWER_LOCATION}/${file_prefix}.${DEV_ID}
    control_file=${CONTROL_ANSWER_LOCATION}/${file_prefix}.${CONTROL_ID}
    if ! [[ -e $dev_file ]]; then
        echo ""
        echo "-----------------------------------"
        echo "$dev_file doesn't exist!"
        echo "-----------------------------------"
        echo ""
        agcm_files_missing=1
    fi
    if ! [[ -e $control_file ]]; then
        echo ""
        echo "-----------------------------------"
        echo "$control_file doesn't exist!"
        echo "-----------------------------------"
        echo ""
        agcm_files_missing=1
    fi
    if (( agcm_files_missing == 0 )); then
        if are_identical $dev_file $control_file; then
            echo ""
            echo "AGCM STATE IDENTICAL"
            echo ""
        else
            echo ""
            echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            echo "             DIFFERENCES IN THE AGCM STATE DETECTED"
            echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            echo ""
            echo ""
            answer_diffs=1
        fi
    fi
fi

# Store answers in the project dir so they can be picked up as artifacts
rm -rf ${CI_PROJECT_DIR}/ci_logs_run_answers_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}
cp -r ${DEV_ANSWER_LOCATION} ${CI_PROJECT_DIR}/ci_logs_run_answers_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}

if (( answer_diffs == 1 )) || (( ocean_files_missing == 1 )) || (( agcm_files_missing == 1 )); then
    # exit with non zero exit status if any problems detected
    exit 1
fi

echo ""
echo "Comparison successful!"
echo ""
